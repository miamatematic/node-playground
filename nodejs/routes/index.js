var express = require('express');
var router = express.Router();

const deezerData = require('../zadatak.json');
var TrackController = require('../controllers/TrackController')

router.get('/', TrackController.getIndex);

router.get('/tracks', TrackController.getAllTracks);

router.get('/tracks/:id', TrackController.getTrack);

router.get('/sorted', TrackController.getSortedTracks)


module.exports = router;
