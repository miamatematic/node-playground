const deezerData = require('../zadatak.json');
const TrackService = require('../services/TrackService');

exports.getIndex = (req, res) => {
  return  res.render('index', { title: 'zadatak' });
}

exports.getAllTracks = (req, res) => {
  const tracks = deezerData.tracks;
  return res.status(200).json(tracks.data);
}

exports.getTrack = (req, res) => {
  const trackId = req.params.id;
  const track = deezerData.tracks.data.find(t => t.id.toString() === trackId);

  if(track) {
    return res.status(200).json(track);
  }
  else {
    return res.sendStatus(404);
  }
}

exports.getSortedTracks = (req, res) => {    
    const {sortBy} = req.query;

    let tracks; 
    if(sortBy === 'name') {
      tracks = TrackService.sortByName(deezerData.tracks.data);
    }
    else if (sortBy === "duration") {
      tracks = TrackService.sortByDuration(deezerData.tracks.data);      
    }
    return res.status(200).json(tracks);
}