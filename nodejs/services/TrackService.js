
exports.sortByDuration = (tracks) => {
   return tracks.sort((track1, track2) => track1.duration-track2.duration);
}

exports.sortByName = (tracks) => {
  return tracks.sort((a, b) => {
    const titleA = a.title.toLowerCase();
    const titleB = b.title.toLowerCase();
    if (titleA < titleB) {
      return -1;
    }
    if (titleA > titleB) {
      return 1;
    }
    return 0;
  });
}