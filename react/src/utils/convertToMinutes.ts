const padWithZeros = (value: number) => {
  const stringValue = value.toString();
  return stringValue.length > 1 ? stringValue : `0${stringValue}`;

}

export const convertToMinutes = (totalSeconds: number) => { 
  const minutes = Math.floor(totalSeconds / 60);
  const seconds = totalSeconds % 60;
  return `${padWithZeros(minutes)}:${padWithZeros(seconds)}`;
}
