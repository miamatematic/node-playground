export interface Album {
  id: number;
  title: string;
  cover_big: string;
}