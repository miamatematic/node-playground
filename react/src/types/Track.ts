import { Artist } from "./Artist";
import { Album } from "./Album";

export interface Track {
  id: number;
  position: number;
  title: string;
  artist: Artist;
  duration: number;
  album: Album
}