import React from 'react';
import Tracks from '../Tracks/index';
import photo from '../../utils/images/photo.jpg';
import icon from '../../utils/images/music-icon.png';
import deezerLogo from '../../utils/images/deezer.jpg';

import style from './Main.module.css';

class Main extends React.Component {
  render() {
    return(
      <div className={style.Container}>
        <div className={style.ContentContainer}>
          <div className={style.Header}>
            <img src={icon} alt="" />
            <h1 className={style.Title}>TOP POP</h1>
            <div className={style.HeaderText}>powered by</div>
            <div className={style.LogoContainer}>
              <img className={style.Logo} src={deezerLogo} alt="" />
            </div>
          </div>
          <div className={style.TracksContainer}>
            <Tracks/>
          </div>
        </div>
        <div>
          <img className={style.StyledImage} src={photo} alt="mainPhoto"/>
        </div>
      </div>
    );
  }
}

export default Main;