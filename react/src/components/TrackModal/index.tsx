import React from 'react';
import Modal from '@material-ui/core/Modal';
import { CardContent, Button } from '@material-ui/core';
import { Track } from '../../types/Track';
import Card from '@material-ui/core/Card'
import { convertToMinutes } from '../../utils/convertToMinutes';

import style from './TrackModal.module.css';

interface Props {
  track: Track,
  isModalOpen: boolean;
  closeModal: () => void;
}

const TrackModal = (props: Props) => {

  const onCloseModal = () => {
    props.closeModal();
  }
  return(
    
    <Modal
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      open={props.isModalOpen}
      onClose={onCloseModal}
      className={style.StyledModal}
    >
      <div>
        <Card className={style.StyledCard}>
          <div className={style.CardContentContainer}>
            <CardContent>
              <div className={style.ModalHeader}>
                  <h1>{props.track.title}</h1>
                  <div className={style.Position}>No.{props.track.position}</div>
              </div>
              <div className={style.ModalBody}>
                <div className={style.InfoContainer}>
                  <div className={style.InfoRow}>
                    <div className={style.Label}>ARTIST:</div> 
                    <div className={style.Text}>{props.track.artist.name}</div>
                    </div>
                  <div className={style.InfoRow}>
                    <div className={style.Label}>DURATION:</div>
                    <div className={style.Text}>{convertToMinutes(props.track.duration)}</div>
                  </div>
                  <div className={style.InfoRow}>
                    <div className={style.Label}>ALBUM:</div>
                    <div className={style.Text}>{props.track.album.title}</div>
                  </div>
                </div>
              </div>
              <div className={style.ModalFooter}>
                <Button 
                  className={style.StyledButton}
                  onClick={onCloseModal}
                >
                  Close
                </Button>
              </div>
            </CardContent>
          </div>
          <img
            src={props.track.album.cover_big}
            alt=""
          />
        </Card>
      </div>
  </Modal>
  );
} 

export default TrackModal;