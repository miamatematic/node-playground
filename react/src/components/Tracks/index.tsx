import React from 'react';
import axios from 'axios';
import { Track } from '../../types/Track';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import TableBody from '@material-ui/core/TableBody';
import { Table } from '@material-ui/core';
import TrackModal from '../TrackModal/index';
import style from './Tracks.module.css';
import { convertToMinutes } from '../../utils/convertToMinutes';

interface State {
  tracks: Track[],
  isAscByDuration: boolean;
  orderByPosition: boolean;
  isAscByPosition: boolean;
  isDurationButtonActive: boolean;
  isPositionButtonActive: boolean;
  isModalOpen: boolean;
  selectedTrack: Track;
}

class Tracks extends React.Component<{}, State>{

  state: State = {
    tracks: [],
    isAscByDuration: false,
    isAscByPosition: true,
    orderByPosition: true,
    isPositionButtonActive: true,
    isDurationButtonActive: false,
    isModalOpen: false,
    selectedTrack: {
      id: 0,
      position: 0,
      title: '',
      artist: {
        id: 0,
        name: ''
      },
      album: {
        id: 0,
        cover_big: '',
        title: ''
      },
      duration: 0
    }
  }

  async componentDidMount() {
    try {
      const response = await axios.get("https://cors-anywhere.herokuapp.com/https://api.deezer.com/chart");
      const deezerTracks = response.data.tracks.data;
      this.setState(() => ({
        tracks: deezerTracks
      }));
    }
    catch {
      alert('Error fetching data');
    }
  }

  onSortDurationClick = () => {
    this.setState((state: State) => ({
      isAscByDuration: !state.isAscByDuration,
      orderByPosition: false,
      isPositionButtonActive: false,
      isDurationButtonActive: true
    }), () => {this.sortTracks()});
  }

  onSortPositionClick = () => {
    this.setState((state: State) => ({
      isAscByPosition: !state.isAscByPosition,
      orderByPosition: true,
      isPositionButtonActive: true,
      isDurationButtonActive: false
    }), () => {this.sortTracks()});
  }

  sortTracks = () => {
    let sortedTracks: Track[] = [];
    if(this.state.isAscByDuration && !this.state.orderByPosition) {
      sortedTracks = this.state.tracks.sort((track1: Track, track2: Track) => track1.duration - track2.duration);
    }
    else if (!this.state.orderByPosition){
      sortedTracks = this.state.tracks.sort((track1: Track, track2: Track) => track2.duration - track1.duration);
    }
    else if (this.state.orderByPosition && this.state.isAscByPosition) {
      sortedTracks = this.state.tracks.sort((track1: Track, track2: Track) => track1.position - track2.position);
    }
    else {
      sortedTracks = this.state.tracks.sort((track1: Track, track2: Track) => track2.position - track1.position);
    }
    this.setState(() => ({
      tracks: sortedTracks
    }))
  }

  openModal = (track: Track) => {
    this.setState(() => ({
      isModalOpen: true,
      selectedTrack: track
    }))
  }

  closeModal = () => {
    this.setState(() => ({
      isModalOpen: false
    }))
  }
  render() {
    const { tracks, isAscByDuration, isAscByPosition, isDurationButtonActive, isPositionButtonActive, isModalOpen,  selectedTrack } = this.state;
    return(
      <div className={style.Container}>
        <Table  aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="right">
                <TableSortLabel
                  active={isPositionButtonActive}
                  direction={isAscByPosition ? 'asc' : 'desc'}
                  onClick={this.onSortPositionClick}
                >
                  Position
                </TableSortLabel>
              </TableCell>
              <TableCell align="right">Title</TableCell>
              <TableCell align="right">
                <TableSortLabel
                  active={isDurationButtonActive}
                  direction={isAscByDuration ? 'asc' : 'desc'}
                  onClick={this.onSortDurationClick}
                >
                Duration
                </TableSortLabel>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {tracks.map((track: Track) => (
              <TableRow key={track.id}  onClick={this.openModal.bind(this, track)}>
                <TableCell component="th" scope="row">
                  {track.position}.
                </TableCell>
                <TableCell align="right">{track.title}</TableCell>
                <TableCell align="right">{convertToMinutes(track.duration)}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <TrackModal track={selectedTrack} isModalOpen={isModalOpen} closeModal = {this.closeModal}/>
      </div>
    );
  }
}

export default Tracks;