import React from 'react';
import { Route } from 'react-router';
import {BrowserRouter} from 'react-router-dom';
import Main from './components/Main';
import './App.css';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Route exact path="/" component={Main} />
      </BrowserRouter>
    </div>
  );
}

export default App;
